# Copyright 2019-2020 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake unpacker

DESCRIPTION="ET:Legacy an open source project based on the code of Wolfenstein: Enemy Territory"
HOMEPAGE="https://www.etlegacy.com/"

SRC_URI="https://github.com/${PN}/${PN}/archive/refs/tags/v${PV}.tar.gz -> ${PN}-${PV}.tar.gz
         pak0pk3? ( http://ftp.gwdg.de/pub/misc/ftp.idsoftware.com/idstuff/et/linux/et-linux-2.60.x86.run )"
LICENSE="GPL-3 pak0pk3? ( ETWOLF )"
SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror bindist"
IUSE="client geoip pak0pk3 omnibot server wolfadmin"

RDEPEND="
    x11-libs/libX11
    media-libs/glew
    media-libs/libsdl2
    media-libs/libjpeg-turbo
    sys-libs/zlib[minizip]
    media-libs/libpng
    net-misc/curl
    dev-libs/openssl
    media-libs/libogg
    media-libs/libtheora
    media-libs/openal[sdl]
    media-libs/freetype
    dev-db/sqlite
    >=dev-lang/lua-5.4.2-r1
"
DEPEND="${RDEPEND}"

src_unpack() {
    use pak0pk3 && unpack_makeself et-linux-2.60.x86.run
    unpack ${PN}-${PV}.tar.gz
}

src_prepare() {
    eapply_user
    sed -i -e 's|-dirty|.${ETLEGACY_VERSION_PATCH}|' cmake/ETLVersion.cmake
    if use pak0pk3; then
        rm -rf "${WORKDIR}"/{bin,CHANGES,Docs,ET.xpm,openurl.sh,pb,README} \
               "${WORKDIR}"/setup.{data,sh} \
               "${WORKDIR}"/etmain/{*.cfg,*.so,*.txt,*.dat,mp_bin.pk3,video}
    fi
    cmake_src_prepare
}

src_configure() {
    local mycmakeargs=(
        -DCMAKE_BUILD_TYPE=Release
        -DCMAKE_INSTALL_PREFIX=/usr \
        -DCMAKE_LIBRARY_PATH=/usr/$(get_libdir) \
        -DINSTALL_DEFAULT_BINDIR=bin \
        -DINSTALL_DEFAULT_MODDIR=/usr/$(get_libdir)/etlegacy \
        -DINSTALL_DEFAULT_BASEDIR=/usr/$(get_libdir)/etlegacy \
        -DCROSS_COMPILE32=OFF \
        -DBUILD_SERVER=$(usex server ON OFF) \
        -DBUILD_CLIENT=$(usex client ON OFF) \
        -DBUILD_MOD=ON \
        -DBUILD_MOD_PK3=ON \
        -DBUILD_PAK3_PK3=ON \
        -DBUNDLED_LIBS=OFF \
        -DBUNDLED_LIBS_DEFAULT=OFF \
        -DFEATURE_AUTOUPDATE=OFF \
        -DFEATURE_IRC_CLIENT=OFF \
        -DFEATURE_IRC_SERVER=OFF \
        -DFEATURE_ANTICHEAT=$(usex server ON OFF) \
        -DFEATURE_SERVERMDX=$(usex server ON OFF) \
        -DINSTALL_EXTRA=ON \
        -DINSTALL_OMNIBOT=$(usex omnibot ON OFF) \
        -DINSTALL_GEOIP=$(usex geoip ON OFF) \
        -DINSTALL_WOLFADMIN=$(usex wolfadmin ON OFF)
    )
    cmake_src_configure
}

src_install() {
    cmake_src_install
    if use pak0pk3; then
        insinto /usr/$(get_libdir)/etlegacy/etmain
        newins "${WORKDIR}"/etmain/pak0.pk3 pak0.pk3
        mkdir -p "${ED}"/usr/share/doc/${PN}-${PV}/license/etmain
        insinto /usr/share/doc/${PN}-${PV}/license/etmain
        newins "${FILESDIR}"/etwolf-license.txt etwolf-license.txt
    fi
}

pkg_postinst() {

    if use pak0pk3; then
        elog ""
        elog "WARNING:"
        elog "You have enabled pak0pk3 USE FLAG which means that"
        elog "Id Software's Enemy Territory Asset pak0.pk3 file"
        elog "has been downloaded and installed on your computer"
        elog "Therefore you have read and accepted the"
        elog "Limited Use Software License Agreement and you are"
        elog "aware of the restriction imposed by the original EULA"
        elog "The etwolf-license.txt file is located in the folder:"
        elog "/usr/share/doc/${PN}-${PV}/license/etmain/"
        elog ""
        elog ""
    fi
}
